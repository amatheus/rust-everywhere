mod utils;

use Core::Core;

use wasm_bindgen::prelude::*;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub struct CoreWeb {
    core: Core
}

#[wasm_bindgen]
impl CoreWeb {
    pub fn new(value_a: f64, value_b: f64) -> CoreWeb {
        return CoreWeb {
            core: Core::new(value_a, value_b)            
        };
    }

    pub fn add(&self) -> f64 {
        return self.core.add();
    }
}