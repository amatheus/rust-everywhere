use Core::Core;

#[no_mangle]
pub extern fn add_numbers(number_a: f64, number_b: f64) -> f64 {
    let core = Core::new(number_a, number_b);
    core.add()
}