use Core::Core;
use jni::JNIEnv;
use jni::objects::{JObject};
use jni::sys::{jdouble};

#[no_mangle]
pub unsafe extern fn Java_com_example_android_MainActivity_hello(
    _env: JNIEnv, 
    _: JObject, 
    value_a: jdouble, value_b: jdouble) -> jdouble {
    
        #[cfg(debug_assertions)]
        println!("Debugging enabled");
    
        #[cfg(not(debug_assertions))]
        println!("Debugging disabled");
    
    #[cfg(debug_assertions)]                
    let core = Core::new(value_a + 3.0, value_b);

    #[cfg(not(debug_assertions))]       
    let core = Core::new(value_a + 8.0, value_b);

    core.add()
}