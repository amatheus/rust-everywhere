//
//  AppleApp.swift
//  Shared
//
//  Created by André Roque Matheus on 18/04/21.
//

import SwiftUI

@main
struct AppleApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
