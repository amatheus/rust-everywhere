.PHONY: setup common coreweb serve coreandroid corec

setup:
	brew install wasm-pack
	rustup target add aarch64-linux-android armv7-linux-androideabi i686-linux-android
	echo Setup/cargo-config >> ~/.cargo/config
	cargo install cbindgen

common:
	cd Common && cargo build

coreweb:
	cd Common/CoreWeb && wasm-pack build

corec:
	cd Common/CoreC && \
		cargo build && \
		cargo lipo && \
		cargo build --release && \
		cargo lipo --release && \
		mkdir -p ../target/universal/debug/include && \
		mkdir -p ../target/universal/release/include && \
		cbindgen src/lib.rs -l c > ../target/universal/debug/include/nodespace.h && \
		cbindgen src/lib.rs -l c > ../target/universal/release/include/nodespace.h 

clean:
	cd Common && cargo clean

serve:
	cd Web && npm run serve

coreandroid:
	cd Common/CoreAndroid && cargo build
	cd Common/CoreAndroid && cargo build --target aarch64-linux-android
	cd Common/CoreAndroid && cargo build --target armv7-linux-androideabi
	cd Common/CoreAndroid && cargo build --target i686-linux-android
	
	cd Common/CoreAndroid && cargo build --release
	cd Common/CoreAndroid && cargo build --target aarch64-linux-android --release
	cd Common/CoreAndroid && cargo build --target armv7-linux-androideabi --release
	cd Common/CoreAndroid && cargo build --target i686-linux-android --release
	
	cp Common/target/release/libCoreAndroid.dylib Java/src/main/resources
	cp Common/target/i686-linux-android/release/libCoreAndroid.so Android/app/src/main/jniLibs/x86
	cp Common/target/armv7-linux-androideabi/release/libCoreAndroid.so Android/app/src/main/jniLibs/armeabi-v7a
	cp Common/target/aarch64-linux-android/release/libCoreAndroid.so Android/app/src/main/jniLibs/arm64-v8a

	cp Common/target/i686-linux-android/debug/libCoreAndroid.so Android/app/src/debug/jniLibs/x86
	cp Common/target/armv7-linux-androideabi/debug/libCoreAndroid.so Android/app/src/debug/jniLibs/armeabi-v7a
	cp Common/target/aarch64-linux-android/debug/libCoreAndroid.so Android/app/src/debug/jniLibs/arm64-v8a
