package com.example.android;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;

public class MainActivity {
    native double hello(double value_a, double value_b);

    static {
        String libName = "/libCoreAndroid.dylib";
        URL url = MainActivity.class.getResource(libName);
        try {
            Path path = Path.of(url.toURI());
            System.load(path.toAbsolutePath().toString());
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String[] args) {
        double result = new MainActivity().hello(1.0, 2.0);
        System.out.println("Result: ");
        System.out.println(result);
    }
}
